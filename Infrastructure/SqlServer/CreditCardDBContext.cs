﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.SqlServer
{
    public class CreditCardDBContext: DbContext
    {
        public CreditCardDBContext() : base("SqlServerEntities")
        {

        }
        public DbSet<Core.Domain.CreditCard> CreditCards { get; set; }
    }
}
