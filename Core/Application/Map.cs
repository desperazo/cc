﻿using AutoMapper;
using Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Application
{
    public class Map : Profile
    {
        public Map()
        {
            CreateMap<CreditCard, CreditCardDto>();
        }
    }
}
