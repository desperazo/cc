﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class CreditCard
    {
        public virtual Guid Id { get; protected set; }
        public virtual string NameOnCard { get; protected set; }
        public virtual string CardNumber { get; protected set; }
        public virtual string Expiry { get; protected set; }

        public static CreditCard Create(string name, string cardNum, string expiry)
        {
            if (string.IsNullOrEmpty(name))
                throw new Exception("Name can't be empty");

            if (string.IsNullOrEmpty(cardNum))
                throw new Exception("Card Number can't be empty");

            if (string.IsNullOrEmpty(expiry))
                throw new Exception("Expiry Date can't be empty");

            CreditCard creditCard = new CreditCard
            {
                Id = Guid.NewGuid(),
                NameOnCard = name,
                CardNumber = cardNum,
                Expiry = expiry
            };

            return creditCard;
        }

        public override bool Equals(object obj)
        {
            CreditCard creditCardToCompare = obj as CreditCard;
            if (creditCardToCompare == null)
                throw new Exception("Can't compare two different objects to each other");

            return this.CardNumber == creditCardToCompare.CardNumber &&
                this.Expiry == creditCardToCompare.Expiry;
        }

        public override int GetHashCode()
        {
            if (String.IsNullOrEmpty(this.CardNumber))
                return 0;

            return this.CardNumber.GetHashCode();
        }
    }
}
