﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Core.Repository;
using Core.Domain;
using System.Linq.Expressions;
using AutoMapper;

namespace Core.Application.Tests
{
    [TestClass]
    public class CreditCardServiceTests
    {
        [TestMethod, TestCategory("Verify Card")]
        public void Visa_StartWiths_4()
        {
            var expected = new VerifyResultDto { CardType = VerifyCardType.Visa };
            var cardRepository = new Mock<IRepository<CreditCard>>();

            CreditCardService serv = new CreditCardService(cardRepository.Object, null);
            var actual = serv.Verify(new CreditCardDto { CardNumber = "4444444444444444", Expiry = "012019", NameOnCard = "firstname lastname" });

            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void MasterCard_StartWiths_5()
        {
            var expected = new VerifyResultDto { CardType = VerifyCardType.MasterCard };

            var cardRepository = new Mock<IRepository<CreditCard>>();

            CreditCardService serv = new CreditCardService(cardRepository.Object, null);
            var actual = serv.Verify(new CreditCardDto { CardNumber = "5444444444444444", Expiry = "012019", NameOnCard = "firstname lastname" });

            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void Amex_StartWiths_3_and_length_15()
        {
            var expected = new VerifyResultDto { CardType = VerifyCardType.Amex };

            var cardRepository = new Mock<IRepository<CreditCard>>();

            CreditCardService serv = new CreditCardService(cardRepository.Object, null);
            var actual = serv.Verify(new CreditCardDto { CardNumber = "344444444444444", Expiry = "012019", NameOnCard = "firstname lastname" });

            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void JCB_StartWiths_3()
        {
            var expected = new VerifyResultDto { CardType = VerifyCardType.JCB };

            var cardRepository = new Mock<IRepository<CreditCard>>();

            CreditCardService serv = new CreditCardService(cardRepository.Object, null);
            var actual = serv.Verify(new CreditCardDto { CardNumber = "3444444444444448", Expiry = "012019", NameOnCard = "firstname lastname" });

            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void Visa_Valid_Leap_Year()
        {
            var expected = new VerifyResultDto
            {
                Status = VerifyStatus.Valid,
                CardType = VerifyCardType.Visa
            };

            var card = new CreditCardDto
            {
                CardNumber = "4444444444444448",
                Expiry = "012020",
                NameOnCard = "firstname lastname"
            };

            var cardRepository = new Mock<IRepository<CreditCard>>();
            cardRepository.Setup(x =>
                    x.FindOne(It.Is<CreditCardExistsSpec>(c => card.CardNumber == "4444444444444448"))
                ).Returns(CreditCard.Create(card.NameOnCard, card.CardNumber, card.Expiry));

            var mapper = new MapperConfiguration(c =>
            {
                c.AddProfile<Core.Application.Map>();
            }).CreateMapper();

            ICreditCardService serv = new CreditCardService(cardRepository.Object, mapper);
            var actual = serv.Verify(card);

            Assert.AreEqual(expected.Status, actual.Status);
            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void MasterCard_Valid_Prime_Year()
        {
            var expected = new VerifyResultDto
            {
                Status = VerifyStatus.Valid,
                CardType = VerifyCardType.MasterCard
            };

            var card = new CreditCardDto
            {
                CardNumber = "5444444444444448",
                Expiry = "012027",
                NameOnCard = "firstname lastname"
            };

            var cardRepository = new Mock<IRepository<CreditCard>>();
            cardRepository.Setup(x =>
                    x.FindOne(It.Is<CreditCardExistsSpec>(c => card.CardNumber == "5444444444444448"))
                ).Returns(CreditCard.Create(card.NameOnCard, card.CardNumber, card.Expiry));

            var mapper = new MapperConfiguration(c =>
            {
                c.AddProfile<Core.Application.Map>();
            }).CreateMapper();

            ICreditCardService serv = new CreditCardService(cardRepository.Object, mapper);
            var actual = serv.Verify(card);

            Assert.AreEqual(expected.Status, actual.Status);
            Assert.AreEqual(expected.CardType, actual.CardType);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void JCB_All_Valid()
        {
            var expected = new VerifyResultDto
            {
                Status = VerifyStatus.Valid,
                CardType = VerifyCardType.JCB
            };

            var card = new CreditCardDto
            {
                CardNumber = "3444444444444448",
                Expiry = "012027",
                NameOnCard = "firstname lastname"
            };

            var cardRepository = new Mock<IRepository<CreditCard>>();
            cardRepository.Setup(x =>
                    x.FindOne(It.Is<CreditCardExistsSpec>(c => card.CardNumber == "3444444444444448"))
                ).Returns(CreditCard.Create(card.NameOnCard, card.CardNumber, card.Expiry));

            var mapper = new MapperConfiguration(c =>
            {
                c.AddProfile<Core.Application.Map>();
            }).CreateMapper();

            ICreditCardService serv = new CreditCardService(cardRepository.Object, mapper);
            var actual = serv.Verify(card);

            Assert.AreEqual(expected.Status, actual.Status);
            Assert.AreEqual(expected.CardType, actual.CardType);
        }


        [TestMethod, TestCategory("Verify Card")]
        public void Card_Not_Exists()
        {
            var expected = new VerifyResultDto
            {
                Status = VerifyStatus.DoesNotExists
            };

            var card = new CreditCardDto
            {
                CardNumber = "3444444444444448",
                Expiry = "012027",
                NameOnCard = "firstname lastname"
            };

            var cardRepository = new Mock<IRepository<CreditCard>>();
            cardRepository.Setup(x =>
                    x.FindOne(It.Is<CreditCardExistsSpec>(c => card.CardNumber == "5444444444444448"))
                ).Returns(CreditCard.Create(card.NameOnCard, card.CardNumber, card.Expiry));

            var mapper = new MapperConfiguration(c =>
            {
                c.AddProfile<Core.Application.Map>();
            }).CreateMapper();

            ICreditCardService serv = new CreditCardService(cardRepository.Object, mapper);
            var actual = serv.Verify(card);

            Assert.AreEqual(expected.Status, actual.Status);
        }

        [TestMethod, TestCategory("Verify Card")]
        public void Rest_are_unknown_or_invalid()
        {
            var expected = new VerifyResultDto
            {
                Status = VerifyStatus.Invalid,
                CardType = VerifyCardType.Unknown
            };

            var card = new CreditCardDto
            {
                CardNumber = "9444444444444448",
                Expiry = "012027",
                NameOnCard = "firstname lastname"
            };

            var cardRepository = new Mock<IRepository<CreditCard>>();
            cardRepository.Setup(x =>
                    x.FindOne(It.Is<CreditCardExistsSpec>(c => card.CardNumber == "9444444444444448"))
                ).Returns(CreditCard.Create(card.NameOnCard, card.CardNumber, card.Expiry));

            var mapper = new MapperConfiguration(c =>
            {
                c.AddProfile<Core.Application.Map>();
            }).CreateMapper();

            ICreditCardService serv = new CreditCardService(cardRepository.Object, mapper);
            var actual = serv.Verify(card);

            Assert.AreEqual(expected.Status, actual.Status);
        }
    }
}