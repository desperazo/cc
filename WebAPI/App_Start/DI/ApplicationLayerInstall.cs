﻿using Castle.MicroKernel.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.App_Start
{
    public class ApplicationLayerInstall : Castle.MicroKernel.Registration.IWindsorInstaller
    {
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Register(Classes.FromAssembly(typeof(Core.Application.ICreditCardService).Assembly)
                .Where(x => !x.IsInterface && !x.IsAbstract && !x.Name.EndsWith("Dto") && x.Namespace.Contains("Application"))
                .WithService.DefaultInterfaces()
                .Configure(c => c.LifestyleTransient()));
        }
    }
}