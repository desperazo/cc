﻿using Core.Repository.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Repository
{
    public interface IRepository<TEntity>
    {
        TEntity FindOne(ISpecification<TEntity> spec);
        void Add(TEntity entity);
    }
}
