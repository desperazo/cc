﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Core.Domain;
using Core.Repository;
using Infrastructure.Memory;
using Infrastructure.SqlServer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAPI.App_Start
{
    public class InfrastructureLayerInstall : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //memory repository
            container.Register(Component.For(typeof(IRepository<>), typeof(MemoryRepository<>)).ImplementedBy(typeof(MemoryRepository<>)).LifestyleSingleton());
            container.Register(Component.For<IRepository<CreditCard>>().ImplementedBy<CreditCardMemoryRepository>().LifestyleSingleton());

            //sql repository
            //container.Register(Component.For<CreditCardDBContext>().LifestylePerWebRequest());
            //container.Register(Component.For<IRepository<CreditCard>>().ImplementedBy<CreditCardSqlServerRepository>().LifestylePerWebRequest());
        }
    }
}