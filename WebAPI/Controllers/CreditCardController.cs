﻿using Core.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CreditCardController : ApiController
    {
        readonly ICreditCardService cardService;

        public CreditCardController(ICreditCardService cardService)
        {
            this.cardService = cardService;
        }

        [HttpGet]
        public Response<CreditCardDto> GetById(string id)
        {
            var response = new Response<CreditCardDto>();
            try
            {
                response.Object = this.cardService.GetByNumber(id);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        [HttpPost]
        public Response<VerifyResultDto> Verify([FromBody] CreditCardDto card)
        {
            var response = new Response<VerifyResultDto>();
            try
            {
                response.Object = this.cardService.Verify(card);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
    }
}
