﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application
{
    public class VerifyResultDto
    {
        public string CardType { get; set; }
        public string Status { get; set; }
    }
}
