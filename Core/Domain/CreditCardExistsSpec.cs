﻿using Core.Repository.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class CreditCardExistsSpec: SpecificationBase<CreditCard>
    {
        string _cardNo;

        public CreditCardExistsSpec(string cardNo)
        {
            this._cardNo = cardNo;
        }

        public override Expression<Func<CreditCard, bool>> SpecExpression
        {
            get
            {
                return card => card.CardNumber == this._cardNo;
            }
        }
    }
}
