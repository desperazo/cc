﻿CREATE PROCEDURE [dbo].[USP_CreditCard_GetByNumber]
	@p_number nvarchar(2)
AS
BEGIN
	SELECT ID, NameOnCard, CardNumber, Expiry
	FROM [dbo].CreditCard
	WHERE CardNumber = @p_number
END
