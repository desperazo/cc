﻿using Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Repository.Specification;
using Core.Domain;

namespace Infrastructure.SqlServer
{
    public class CreditCardSqlServerRepository : IRepository<Core.Domain.CreditCard>
    {
        CreditCardDBContext _repository;
        public CreditCardSqlServerRepository(CreditCardDBContext repository)
        {
            _repository = repository;
        }

        public void Add(Core.Domain.CreditCard entity)
        {
            _repository.CreditCards.Add(entity);
            _repository.SaveChanges();
        }

        public Core.Domain.CreditCard FindOne(ISpecification<Core.Domain.CreditCard> spec)
        {
            return _repository.CreditCards.SingleOrDefault(spec.SpecExpression);
        }
    }
}
