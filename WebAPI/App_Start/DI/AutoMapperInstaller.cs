﻿using AutoMapper;
using AutoMapper.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.App_Start
{
    public class AutoMapperInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IMapper>().UsingFactoryMethod(x =>
                {
                    return new MapperConfiguration(c =>
                    {
                        c.AddProfile<Core.Application.Map>();
                    }).CreateMapper();
                }));
        }
    }
}