﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application
{
    public class VerifyCardType
    {
        public const string Visa = "Visa";
        public const string MasterCard = "MasterCard";
        public const string Amex = "Amex";
        public const string JCB = "JCB";
        public const string Unknown = "Unknown";
    }
}
