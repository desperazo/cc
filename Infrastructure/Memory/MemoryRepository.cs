﻿using Core.Repository;
using Core.Repository.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Memory
{
    public class MemoryRepository<TEntity> : IRepository<TEntity>
    {
        protected static List<TEntity> entities = new List<TEntity>();

        public TEntity FindOne(ISpecification<TEntity> spec)
        {
            return entities.Where(spec.IsSatisfiedBy).FirstOrDefault();
        }

        public void Add(TEntity entity)
        {
            entities.Add(entity);
        }
    }
}
