﻿using Core.Domain;
using Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Repository.Specification;

namespace Infrastructure.Memory
{
    public class CreditCardMemoryRepository : IRepository<CreditCard>
    {
        MemoryRepository<CreditCard> _repository;
        public CreditCardMemoryRepository(MemoryRepository<CreditCard> memRepository)
        {
            this._repository = memRepository;

            //add fake
            Add(CreditCard.Create("amax", "300000000000000", "062019"));
            Add(CreditCard.Create("jcb", "3000000000000000", "062019"));
            Add(CreditCard.Create("visa", "4000000000000000", "062019"));
            Add(CreditCard.Create("master", "5000000000000000", "062019"));
        }

        public void Add(CreditCard entity)
        {
            this._repository.Add(entity);
        }

        public CreditCard FindOne(ISpecification<CreditCard> spec)
        {
            return this._repository.FindOne(spec);
        }
    }
}
