﻿using AutoMapper;
using Core.Domain;
using Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core.Application
{
    public class CreditCardService : ICreditCardService
    {
        readonly IRepository<CreditCard> _creditCardRepository;
        private readonly IMapper _mapper;

        public CreditCardService(IRepository<CreditCard> creditCardRepository, IMapper mapper)
        {
            this._creditCardRepository = creditCardRepository;
            this._mapper = mapper;
        }

        public CreditCardDto GetByNumber(string cardNumber)
        {
            var card = _creditCardRepository.FindOne(new CreditCardExistsSpec(cardNumber));
            if (card == null)
                return null;

            return _mapper.Map<CreditCard, CreditCardDto>(card);
        }

        public VerifyResultDto Verify(CreditCardDto card)
        {
            string cardType = VerifyCardType.Unknown;
            string status = VerifyStatus.Invalid;

            Regex reg = new Regex(@"\d{15,16}");
            Regex regExpiry = new Regex(@"^(0[1-9]|1[0-2])(?<year>20[0-9][0-9])$");
            if (!reg.IsMatch(card.CardNumber) || !regExpiry.IsMatch(card.Expiry))
            {
                return new VerifyResultDto
                {
                    CardType = cardType,
                    Status = status
                };
            }

            switch (card.CardNumber[0])
            {
                case '4':
                    cardType = VerifyCardType.Visa;
                    break;
                case '5':
                    cardType = VerifyCardType.MasterCard;
                    break;
                case '3':
                    if (card.CardNumber.Length == 15)
                        cardType = VerifyCardType.Amex;
                    if (card.CardNumber.Length == 16)
                        cardType = VerifyCardType.JCB;
                    break;
            }

            if (GetByNumber(card.CardNumber) == null)
            {
                status = VerifyStatus.DoesNotExists;
            }
            else
            {
                Match match = regExpiry.Match(card.Expiry);
                int year = int.Parse(match.Groups["year"].Value);
                status = VerifyCardStatus(cardType, year);
            }

            return new VerifyResultDto
            {
                CardType = cardType,
                Status = status
            };
        }

        private string VerifyCardStatus(string cardType, int year)
        {
            if (cardType == VerifyCardType.Visa)
                return year % 4 == 0 ? VerifyStatus.Valid : VerifyStatus.Invalid;
            if(cardType == VerifyCardType.MasterCard)
                return IsPrime(year) ? VerifyStatus.Valid : VerifyStatus.Invalid;
            if (cardType == VerifyCardType.JCB)
                return VerifyStatus.Valid;
            return VerifyStatus.Invalid;
        }

        private bool IsPrime(int number)
        {
            if (number == 1)
                return false;
            if (number == 2)
                return true;

            for (int i = 2; i <= Math.Ceiling(Math.Sqrt(number)); ++i)
            {
                if (number % i == 0)
                    return false;
            }
            return true;
        }
    }
}
