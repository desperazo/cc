﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application
{
    public class VerifyStatus
    {
        public const string Valid = "Valid";
        public const string Invalid = "Invalid";
        public const string DoesNotExists = "DoesNotExists";
    }
}
