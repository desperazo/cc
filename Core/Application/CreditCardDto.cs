﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application
{
    public class CreditCardDto
    {
        public string NameOnCard { get; set; }
        public string CardNumber { get; set; }
        public string Expiry { get; set; }
    }
}
